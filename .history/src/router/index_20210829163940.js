import viewPage from "../view/PageWeb/index.vue";
import Home from "../view/PageWeb/homePage/homePage.vue";
import product from "../view/PageWeb/productDetails/product.vue";
import login from "../view/PageWeb/login/login.vue";
import register from "../view/PageWeb/register/register.vue";
import cart from "../view/PageWeb/cart/cart.vue";

// =========== Page Admin ================
import adminPage from "../view/PageAdmin/homeAdmin.vue";
import adminBook from "../view/PageAdmin/homeAdmin/adminbook/adminbook.vue";
import adminAuthor from "../view/PageAdmin/homeAdmin/admin-author/adminauthor.vue";
import adminPublisher from "../view/PageAdmin/homeAdmin/admin-publisher/adminpublisher.vue";
import adminOrder from "../view/PageAdmin/homeAdmin/admin-order/adminorder.vue";
import adminUser from "../view/PageAdmin/homeAdmin/admin-user/adminuser.vue"

export const routes = [{
        path: "/",
        component: viewPage,
        children: [{
                path: "",
                name: "Home",
                component: Home,
            },
            {
                path: "product",
                name: "product",
                component: product,
            },
            {
                path: "login",
                name: "login",
                component: login,
            },
            {
                path: "register",
                name: "register",
                component: register,
            },
            {
                path: "cart",
                name: "cart",
                component: cart,
            },
        ],
    },
    {
        path: "/admin",
        component: adminPage,
        children: [{
                path: "",
                name: "admin-book",
                component: adminBook,
            },
            {
                path: "admin-author",
                name: "adminauthor",
                component: adminAuthor,
            },
            {
                path: "admin-publisher",
                name: "adminPublisher",
                component: adminPublisher,
            },
            {
                path: "admin-order",
                name: "adminOrder",
                component: adminOrder,
            },
            {
                path: "admin-user",
                name: "adminuser",
                component: adminUser,
            },
        ],
    },
];