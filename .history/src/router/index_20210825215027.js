import viewPage from "../view/PageWeb/index.vue";
import Home from "../view/PageWeb/homePage/homePage.vue";
import product from "../view/PageWeb/productDetails/product.vue";
import login from "../view/PageWeb/login/login.vue";
import register from "../view/PageWeb/register/register.vue";
import cart from "../view/PageWeb/cart/cart.vue";

// =========== Page Admin ================
import adminPage from "../view/PageAdmin/homeAdmin.vue";
import adminBook from "../view/PageAdmin/homeAdmin/adminbook/adminbook.vue";
import adminAuthor from "../view/PageAdmin/homeAdmin/admin-author/adminauthor.vue";
import adminPublisher from "../view/PageAdmin/homeAdmin/admin-publisher/adminpublisher.vue";

export const routes = [{
        path: "/",
        component: viewPage,
        children: [{
                path: "",
                name: "Home",
                component: Home,
            },
            {
                path: "product",
                name: "product",
                component: product,
            },
            {
                path: "login",
                name: "login",
                component: login,
            },
            {
                path: "register",
                name: "register",
                component: register,
            },
            {
                path: "cart",
                name: "cart",
                component: cart,
            },
        ],
    },
    {
        path: "/admin",
        component: adminPage,
        children: [{
                path: "adminbook",
                name: "adminbook",
                component: adminBook,
            },
            {
                path: "adminauthor",
                name: "adminauthor",
                component: adminAuthor,
            },
            {
                path: "adminPublisher",
                name: "adminPublisher",
                component: adminPublisher,
            },
        ],
    },
];