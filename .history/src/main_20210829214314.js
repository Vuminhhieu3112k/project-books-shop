import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import { routes } from './router'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
//css
import "./assets/css/reset.css";
import "./assets/css/base.css";
import "./assets/css/main.css";
import "./assets/css/product_detail.css";
import "./assets/css/login-regis.css";
import "./assets/css/cart.css";
import "./assets/css/grid.css";
import "./assets/css/responsive.css";
import "./assets/css/adminlte.css";
import "./assets/css/addtable.css"




library.add(fas);
Vue.component('fa', FontAwesomeIcon)
Vue.config.productionTip = false
Vue.use(VueRouter)

const router = new VueRouter({
    mode: "history",
    routes
})
new Vue({
    router: router,
    render: h => h(App),
}).$mount('#app')